var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var port = 3000;

// convertir peticiones a objeto json
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.get('/', function(req, res){
	res.status(200).send('201909103');
});

app.post('/sumar', function(req, res){
    body = req.body;
    numero1 = body.numero1;
    numero2 = body.numero2;
	res.status(200).send(String(numero1 + numero2));
});

app.post('/restar', function(req, res){
    body = req.body;
    numero1 = body.numero1;
    numero2 = body.numero2;
	res.status(200).send(String(numero1 - numero2));
});

// para hotfix
app.post('/multiplicar', function(req, res){
    body = req.body;
    numero1 = body.numero1;
    numero2 = body.numero2;
	res.status(200).send(String(numero1 * numero2));
});


app.listen(port, function(){
	console.log(`Corriendo en puerto ${port}`);
});