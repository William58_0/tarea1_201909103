var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var port = 4000;

// convertir peticiones a objeto json
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.get('/', function(req, res){
	res.status(200).send('201909103');
});

app.get('/info', function(req, res){
    res.status(200).send('William Alejandro Borrayo Alarcón - 201909103');
});


app.listen(port, function(){
	console.log(`Corriendo en puerto ${port}`);
});